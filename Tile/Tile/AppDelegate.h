//
//  AppDelegate.h
//  Tile
//
//  Created by Timotin Vanea on 3/14/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flurry.h"
#import "GameCenterManager.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@end
