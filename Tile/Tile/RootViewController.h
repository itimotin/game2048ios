//
//  RootViewController.h
//  Tile
//
//  Created by Timotin Vanea on 3/26/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GADBannerViewDelegate.h"
@class GADBannerView, GADRequest;

@interface RootViewController : UIViewController<GADBannerViewDelegate>{

}
@property(strong, nonatomic) GADBannerView *adBanner;

- (GADRequest *)createRequest;

@end
