//
//  RootViewController.m
//  Tile
//
//  Created by Timotin Vanea on 3/26/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "RootViewController.h"
#import "GADBannerView.h"
#import "GADRequest.h"
@interface RootViewController ()

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSLog(@"aici");
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    //Ads
    self.adBanner = [[GADBannerView alloc]
                     initWithAdSize:((UIInterfaceOrientationIsPortrait(currentOrientation))?kGADAdSizeSmartBannerPortrait:kGADAdSizeSmartBannerLandscape)];
    
    // Need to set this to no since we're creating this custom view.
    self.adBanner.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID
    // before compiling.
    self.adBanner.adUnitID = MY_BANNER_UNIT_ID;
    self.adBanner.delegate = self;
    [self.adBanner setRootViewController:self];
    [self.view addSubview:self.adBanner];
    [self.adBanner loadRequest:[self createRequest]];
    
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.adBanner
                                  attribute:NSLayoutAttributeBottom
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeBottom
                                 multiplier:1.0
                                   constant:0]];
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.adBanner
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeCenterX
                                 multiplier:1.0
                                   constant:0]];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark GADRequest generation

// Here we're creating a simple GADRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (GADRequest *)createRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
    request.testDevices = [NSArray arrayWithObjects:@"af028767f1fdd1d565451af4c6b384c2", nil];
    return request;
}

#pragma mark GADBannerViewDelegate callbacks

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    // Ignoring specific orientations we don't care about.
    if (orientation == UIDeviceOrientationFaceUp ||
        orientation == UIDeviceOrientationFaceDown ||
        orientation == UIDeviceOrientationUnknown) {
        return;
    }
    // On orientation changes we do care about, remove the Smart Banner entry and
    // replace it with the Smart Banner entry correct for the orientation we are
    // in.
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        self.adBanner.adSize = kGADAdSizeSmartBannerLandscape;
    } else if (UIInterfaceOrientationIsPortrait(orientation)) {
        self.adBanner.adSize = kGADAdSizeSmartBannerPortrait;
    }
    
}


@end
