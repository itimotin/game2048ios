//
//  ViewController.h
//  Tile
//
//  Created by Timotin Vanea on 3/14/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>

#import "GameCenterManager.h"
#import "GADBannerViewDelegate.h"
@class GADBannerView, GADRequest;

@interface ViewController : UIViewController<UIGestureRecognizerDelegate
, UIAlertViewDelegate,GameCenterManagerDelegate,GADBannerViewDelegate> {

}
@property (strong, nonatomic) IBOutlet UIView *workingView;
@property (nonatomic, strong) IBOutlet UISwipeGestureRecognizer *swipeLeftRecognizer;
@property(strong, nonatomic) GADBannerView *adBanner;

- (GADRequest *)createRequest;
- (void)resetAction;
@end
