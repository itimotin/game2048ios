//
//  ViewController.m
//  Tile
//
//  Created by Timotin Vanea on 3/14/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import "ViewController.h"
#import "Flurry.h"
#import "GADInterstitial.h"
#import "GADBannerView.h"
#import "GADRequest.h"

@interface ViewController (){
    NSUInteger myIntArr[5][5];
    int8_t arrEfect[5][5];
    int fourCnt;
    NSUInteger totalScore;
    BOOL isChanged;
    UInt16 iMAXMAL;
}

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Set GameCenter Manager Delegate
    [[GameCenterManager sharedManager] setDelegate:self];
    iMAXMAL = 3;
    
    fourCnt = 0;
    totalScore = 0;
//    labelEfect.font = labelScore.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:(IS_IPAD)?40:24];
    isChanged = YES;
    [self allWithZero];
    [self addRectsOnView];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view addGestureRecognizer:self.swipeLeftRecognizer];
    
    UIInterfaceOrientation currentOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    //Ads
    self.adBanner = [[GADBannerView alloc]
                     initWithAdSize:((UIInterfaceOrientationIsPortrait(currentOrientation))?kGADAdSizeSmartBannerPortrait:kGADAdSizeSmartBannerLandscape)];
    
    // Need to set this to no since we're creating this custom view.
    self.adBanner.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Note: Edit SampleConstants.h to provide a definition for kSampleAdUnitID
    // before compiling.
    self.adBanner.adUnitID = MY_BANNER_UNIT_ID;
    self.adBanner.delegate = self;
    [self.adBanner setRootViewController:self];
    [self.view addSubview:self.adBanner];
    [self.adBanner loadRequest:[self createRequest]];
    
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.adBanner
                                  attribute:NSLayoutAttributeBottom
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeBottom
                                 multiplier:1.0
                                   constant:0]];
    [self.view addConstraint:
     [NSLayoutConstraint constraintWithItem:self.adBanner
                                  attribute:NSLayoutAttributeCenterX
                                  relatedBy:NSLayoutRelationEqual
                                     toItem:self.view
                                  attribute:NSLayoutAttributeCenterX
                                 multiplier:1.0
                                   constant:0]];
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStylePlain target:self action:@selector(resetAction)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    BOOL available = [[GameCenterManager sharedManager] checkGameCenterAvailability];
    if (available) {
        NSLog(@"Avaible");
    } else {
        NSLog(@"Unavaible");
    }
    
    GKLocalPlayer *player = [[GameCenterManager sharedManager] localPlayerData];
    if (player) {
        if ([player isUnderage] == NO) {
            NSLog(@"%@, signed in.", player.displayName);
            [[GameCenterManager sharedManager] localPlayerPhoto:^(UIImage *playerPhoto) {
            }];
        } else {
            NSLog(@"Player is underage");
        }
    } else {
       NSLog(@"No GameCenter player found.");
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) addRectsOnView
{
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            UILabel *lblr = [[UILabel alloc] initWithFrame:CGRectMake(5+i*self.workingView.frame.size.width/iMAXMAL, 5+j*self.workingView.frame.size.height/iMAXMAL, self.workingView.frame.size.width/iMAXMAL-10, self.workingView.frame.size.height/iMAXMAL-10)];
            lblr.textAlignment = NSTextAlignmentCenter;
            lblr.backgroundColor = [UIColor redColor];
            lblr.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:(IS_IPAD)?40:20];
            lblr.tag = i*10+j+1;
            
            if (myIntArr[i][j]!=0) {
                lblr.backgroundColor = [self getBackgroudForNum:(int)myIntArr[i][j]];
                lblr.text = [NSString stringWithFormat:@"%ld", (unsigned long)myIntArr[i][j]];
            }else{
                lblr.backgroundColor = AlphaUIColorFromRGB(0xffffff);
                lblr.text = @"";
            }
            
            [self.workingView addSubview:lblr];
        }
    }
}

- (UIColor *)getBackgroudForNum:(int)num
{
    if (num == 2) return UIColorFromRGB(0xede4db);
    if (num == 4) return UIColorFromRGB(0xFFF1EC);
    if (num == 8) return UIColorFromRGB(0xFFDFD4);
    if (num == 16) return UIColorFromRGB(0xFED2C2);
    if (num == 32) return UIColorFromRGB(0xFEC4AF);
    if (num == 64) return UIColorFromRGB(0xFFB196);
    if (num == 128) return UIColorFromRGB(0xFF9D7B);
    if (num == 256) return UIColorFromRGB(0xFF865C);
    if (num == 512) return UIColorFromRGB(0xFF6D3A);
    if (num == 1024) return UIColorFromRGB(0xE1FFFD);
    if (num == 2048) return UIColorFromRGB(0x96E2F5);
    if (num == 4096) return UIColorFromRGB(0x4AD4F6);
    if (num == 8192) return UIColorFromRGB(0x42CEC2);
    return [UIColor blueColor];
}

- (void)changeRect
{
    int tempScore = 0;
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            UILabel *lbl = (UILabel*)[self.workingView viewWithTag:i*10+j+1];
            
            if (myIntArr[i][j]!=0) {
                if (myIntArr[i][j] > 512) {
                    lbl.font = lbl.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:(IS_IPAD)?40:20];
                }
                lbl.text = [NSString stringWithFormat:@"%ld", (unsigned long)myIntArr[i][j]];
                lbl.backgroundColor = [self getBackgroudForNum:(int)myIntArr[i][j]];
            }else{
                lbl.backgroundColor = AlphaUIColorFromRGB(0xffffff);
                lbl.text = @"";
            }
            
            if (arrEfect[i][j]!=0) {
                float scale = 0.0 ;
                if (arrEfect[i][j] > 1) {
                    lbl.transform = CGAffineTransformMakeScale(0.4, 0.6);
                    scale = 0.8;
                }else{
                    lbl.transform = CGAffineTransformMakeScale((IS_IPAD)?1.1:1.2, (IS_IPAD)?1.1:1.2);
                    scale = 0.8;
                    tempScore = tempScore + (int)myIntArr[i][j];
                }
                
                arrEfect[i][j]=0;
                
                [UIView animateWithDuration:0.15 delay:0.05 options:UIViewAnimationOptionCurveLinear animations:^{lbl.transform = CGAffineTransformMakeScale(scale, scale);
                } completion:^(BOOL finished){
                    [UIView animateWithDuration:0.15 animations:^{
                        lbl.transform = CGAffineTransformMakeScale(1, 1);
                    }];
                    
                }];
            }
        }
    }
    if (tempScore > 0) {
        totalScore = totalScore + tempScore;
        self.title = [NSString stringWithFormat:@"%lu",(unsigned long)totalScore];
    }
}
#pragma mark GESTURE DELEGATE
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return YES;
}


#pragma mark - GESTURE RECOGNIZER ACCTION

- (IBAction)showGestureForSwipeRecognizer:(UISwipeGestureRecognizer *)recognizer {
    [self.view removeGestureRecognizer:self.swipeLeftRecognizer];
    if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        [self left];
        [self leftCalc];
        [self left];
    }
    else if(recognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        [self right];
        [self rightCalc];
        [self right];
    } else if(recognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        [self up];
        [self upCalc];
        [self up];
    }
    else {
        [self down];
        [self downCalc];
        [self down];
    }
    if (iMAXMAL!=5) {
        [self controlAboutMaximalValue];
    }
    [self changeRect];
    
    [self performSelector:@selector(addNumber) withObject:self afterDelay:.1];
   
}

#pragma mark - MAXIMIZE MATRIX
- (void) controlAboutMaximalValue
{
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j] == [self getMaximValue]) {
                if (iMAXMAL<5) {
                    iMAXMAL++;
                    [self maximizeMatrix];
                    [self performSelector:@selector(addIntersittial) withObject:self afterDelay:10.0];
                }
            }
        }
    }
}

- (void)maximizeMatrix {
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            UILabel *lbl = (UILabel*)[self.workingView viewWithTag:i*10+j+1];
            if (lbl == nil) {
                UILabel *lblr = [[UILabel alloc] initWithFrame:CGRectMake(5+i*self.workingView.frame.size.width/iMAXMAL, 5+j*self.workingView.frame.size.height/iMAXMAL, self.workingView.frame.size.width/iMAXMAL-10, self.workingView.frame.size.height/iMAXMAL-10)];
                lblr.textAlignment = NSTextAlignmentCenter;
                lblr.backgroundColor = [UIColor redColor];
                lblr.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:(IS_IPAD)?40:20];
                lblr.tag = i*10+j+1;
                
                if (myIntArr[i][j]!=0) {
                    lblr.backgroundColor = [self getBackgroudForNum:(int)myIntArr[i][j]];
                    lblr.text = [NSString stringWithFormat:@"%ld", (unsigned long)myIntArr[i][j]];
                }else{
                    lblr.backgroundColor = AlphaUIColorFromRGB(0xffffff);
                    lblr.text = @"";
                }
                
                [self.workingView addSubview:lblr];
            }
            else
            {
                lbl.frame = CGRectMake(5+i*self.workingView.frame.size.width/iMAXMAL, 5+j*self.workingView.frame.size.height/iMAXMAL, self.workingView.frame.size.width/iMAXMAL-10, self.workingView.frame.size.height/iMAXMAL-10);
            }
            
        }
    }

}

- (uint_least32_t)getMaximValue
{
    switch (iMAXMAL) {
        case 3:
            return 128;
            break;
        case 4:
            return 2048;
            break;
        case 5:
            return 4096;
            break;
        default:
            break;
    }
    return 8192;
}

- (void)resetAction{
    
    NSMutableArray* arr = [self colectWithZero];
    if (arr.count == 0 && [self controlIsGameOver]) {
        [self resetNow];
    }else{
        [self.view removeGestureRecognizer:self.swipeLeftRecognizer];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset menu" message:@"Do You want to reset game?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        [alert show];
    }
    
}

- (void) addIntersittial
{
    GADInterstitial *splashInterstitial_ = [[GADInterstitial alloc] init];
    splashInterstitial_.adUnitID = MY_INTERSTITIAL_UNIT_ID;
    [splashInterstitial_ loadAndDisplayRequest:[GADRequest request]
                                   usingWindow:self.view.window
                                  initialImage:[UIImage imageNamed:@"Default.png"]];
    self.navigationItem.leftBarButtonItem.tintColor = self.navigationItem.rightBarButtonItem.tintColor = [UIColor blueColor];
}

#pragma mark - DREAPTA
- (void)right
{
    for (int i = iMAXMAL-1; i>=0; i--) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]==0){
                for (int k = 1 ; k <iMAXMAL; k++) {
                    if (i-k>=0) {
                        if (myIntArr[i-k][j]!=0) {
                            myIntArr[i][j] = myIntArr[i-k][j];
                            myIntArr[i-k][j] = 0;
                            arrEfect[i][j] = arrEfect[i-k][j];
                            arrEfect[i-k][j] = 0;
                            isChanged = YES;
                            k = iMAXMAL;
                        }
                    }
                }
            }
        }
    }
}


- (void) rightCalc{
    for (int i = iMAXMAL-1; i>=0; i--) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]!=0) {
                if (i<iMAXMAL-1) {
                    if (myIntArr[i+1][j]==myIntArr[i][j]) {
                        myIntArr[i+1][j] = myIntArr[i+1][j] + myIntArr[i][j];
                        myIntArr[i][j] = 0;
                        arrEfect[i+1][j] = 1;
                        isChanged = YES;
                    }
                }
            }
        }
    }
}

#pragma mark - Stinga
- (void)left
{
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]==0){
                for (int k = 1 ; k <iMAXMAL; k++) {
                    if (i<k) {
                        if (myIntArr[k][j]!=0) {
                            myIntArr[i][j] = myIntArr[k][j];
                            myIntArr[k][j] = 0;
                            arrEfect[i][j] = arrEfect[k][j];
                            arrEfect[k][j] = 0;
                            isChanged = YES;
                            k = iMAXMAL;
                        }
                    }
                }
            }
        }
    }
}

- (void)leftCalc {
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]!=0) {
                if (i>0) {
                    if (myIntArr[i-1][j]==myIntArr[i][j]) {
                        myIntArr[i-1][j] = myIntArr[i-1][j] + myIntArr[i][j];
                        myIntArr[i][j] = 0;
                        arrEfect[i-1][j] = 1;
                        isChanged = YES;
                    }
                }
            }
        }
    }
}

#pragma mark - SUS
- (void) up
{
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]==0){
                for (int k = 1 ; k <iMAXMAL; k++) {
                    if (j<k) {
                        if (myIntArr[i][k]!=0) {
                            myIntArr[i][j] = myIntArr[i][k];
                            myIntArr[i][k] = 0;
                            arrEfect[i][j] = arrEfect[i][k];
                            arrEfect[i][k] = 0;
                            isChanged = YES;
                            k = iMAXMAL;
                            
                        }
                    }
                }
            }
        }
    }
}

- (void)upCalc {
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]!=0) {
                if (j > 0) {
                    if (myIntArr[i][j-1]==myIntArr[i][j]) {
                        myIntArr[i][j-1] = myIntArr[i][j-1] + myIntArr[i][j];
                        myIntArr[i][j] = 0;
                        arrEfect[i][j-1] = 1;
                        isChanged = YES;
                    }
                }
            }
        }
    }
}

#pragma mark - JOS
- (void) down
{
    for (int i = iMAXMAL-1; i>=0; i--) {
        for (int j = iMAXMAL-1; j>=0; j--) {
            if (myIntArr[i][j]==0){
                for (int k = 1 ; k < iMAXMAL; k++) {
                    if (j-k>=0) {
                        if (myIntArr[i][j-k]!=0) {
                            myIntArr[i][j] = myIntArr[i][j-k];
                            myIntArr[i][j-k] = 0;
                            arrEfect[i][j] = arrEfect[i][j-k];
                            arrEfect[i][j-k] = 0;
                            isChanged = YES;
                            k = iMAXMAL;
                            
                        }
                    }
                }
            }
        }
    }
}

- (void) downCalc {
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = iMAXMAL; j>=0; j--) {
            if (myIntArr[i][j]!=0) {
                
                if (j<iMAXMAL) {
                    if (myIntArr[i][j+1]==myIntArr[i][j]) {
                        myIntArr[i][j+1] = myIntArr[i][j+1] + myIntArr[i][j];
                        myIntArr[i][j] = 0;
                        arrEfect[i][j+1] = 1;
                        isChanged = YES;
                    }
                }
            }
        }
        
    }
}

- (NSMutableArray*)colectWithZero
{
    NSMutableArray* arr = [NSMutableArray array];
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]==0) {
                [arr addObject:[NSValue valueWithCGPoint:CGPointMake(i, j)]];
            }
        }
    }
    return arr;
}

- (void) addNumber{
    NSMutableArray* arr = [self colectWithZero];
    if (arr.count == 0 && [self controlIsGameOver]) {
        [self addScoreToGameCenter];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"%d", totalScore] message:@"Do You want to try again?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"YES", nil];
        [alert show];
        return;
    }else{
        if (arr.count > 0) {
            if (isChanged) {
                NSInteger i = arc4random()%arr.count;
                CGPoint point = [[arr objectAtIndex:i] CGPointValue];
                BOOL isFour = arc4random() % 2 == 0;
                NSUInteger num = 2;
                if (isFour) {
                    fourCnt++;
                    if (fourCnt == 2) {
                        fourCnt = 0;
                        num = 4;
                    }
                }
                arrEfect[(int)point.x][(int)point.y] = myIntArr[(int)point.x][(int)point.y] = num;
                [self changeRect];
            }
            
        }
    }
    isChanged = NO;
    [self.view addGestureRecognizer:self.swipeLeftRecognizer];
}

- (BOOL)controlIsGameOver{
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            if (myIntArr[i][j]!=0) {
                if (j > 0) {
                    if (myIntArr[i][j-1]==myIntArr[i][j]) {
                        return NO;
                    }
                }
                
                if (i>0) {
                    if (myIntArr[i-1][j]==myIntArr[i][j]) {
                        return NO;
                    }
                }
            }
        }
    }
    return YES;
}

#pragma mark - ALERT VIEW
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.view addGestureRecognizer:self.swipeLeftRecognizer];
    switch (buttonIndex) {
        case 0:
        {
            NSLog(@"cancel");
            [self addIntersittial];
            [Flurry logEvent:@"Canceled"];
        }
            break;
            
        case 1:
            [self resetNow];
            break;
        default:
            break;
    }
}

- (void)resetNow
{
    [self removeLabelsFromScreen];
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            myIntArr[i][j] = 0;
            arrEfect[i][j] = 0;
        }
    }
    iMAXMAL = 3;
    totalScore = 0;
    [self allWithZero];
    [self addRectsOnView];
    [Flurry logEvent:@"reseted"];
}

- (void)removeLabelsFromScreen
{
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            UILabel *lbl = (UILabel*)[self.workingView viewWithTag:i*10+j+1];
            [lbl removeFromSuperview];
        }
    }
}

- (void)allWithZero
{
   
    for (int i = 0; i<iMAXMAL; i++) {
        for (int j = 0; j<iMAXMAL; j++) {
            myIntArr[i][j] = 0;
            arrEfect[i][j] = 0;
        }
    }
    for (int i = 0; i<2; i++) {
         isChanged = YES;
        [self addNumber];
    }
    self.title = [NSString stringWithFormat:@"%lu",(unsigned long)totalScore];
}


#pragma mark - Game Center
- (void)addScoreToGameCenter
{
    [[GameCenterManager sharedManager] saveAndReportScore:totalScore leaderboard:@"Score" sortOrder:GameCenterSortOrderHighToLow];
    [self performSelector:@selector(addLeadBoard) withObject:self afterDelay:0.5];
}

- (void) addLeadBoard {
    BOOL available = [[GameCenterManager sharedManager] checkGameCenterAvailability];
    if (available) {
        NSLog(@"Avaible");
        [[GameCenterManager sharedManager] presentLeaderboardsOnViewController:self];
    } else {
        NSLog(@"Unavaible");
    }

   
}

#pragma mark - GameKit Delegate

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    if (gameCenterViewController.viewState == GKGameCenterViewControllerStateAchievements) {
        NSLog(@"Displayed GameCenter achievements.");
    } else if (gameCenterViewController.viewState == GKGameCenterViewControllerStateLeaderboards) {
        NSLog(@"Displayed GameCenter leaderboard.");
    } else {
        NSLog(@"Displayed GameCenter controller.");
    }
}

#pragma mark - GameCenter Manager Delegate

- (void)gameCenterManager:(GameCenterManager *)manager authenticateUser:(UIViewController *)gameCenterLoginController {
    [self presentViewController:gameCenterLoginController animated:YES completion:^{
        NSLog(@"Finished Presenting Authentication Controller");
    }];
}

- (void)gameCenterManager:(GameCenterManager *)manager availabilityChanged:(NSDictionary *)availabilityInformation {
    NSLog(@"GC Availabilty: %@", availabilityInformation);
    if ([[availabilityInformation objectForKey:@"status"] isEqualToString:@"GameCenter Available"]) {
       NSLog(@"Game Center is online, the current player is logged in, and this app is setup.");
    } else {
        NSLog(@"error");
    }
    
    GKLocalPlayer *player = [[GameCenterManager sharedManager] localPlayerData];
    if (player) {
        if ([player isUnderage] == NO) {
            NSLog(@"%@ signed in.", player.displayName);
            [[GameCenterManager sharedManager] localPlayerPhoto:^(UIImage *playerPhoto) {
            }];
        } else {
            NSLog(@"Player is underage");
        }
    } else {
        NSLog(@"No GameCenter player found.");
    }
}

- (void)gameCenterManager:(GameCenterManager *)manager error:(NSError *)error {
    NSLog(@"GCM Error: %@", error);
}

- (void)gameCenterManager:(GameCenterManager *)manager reportedAchievement:(GKAchievement *)achievement withError:(NSError *)error {
    if (!error) {
        NSLog(@"GCM Reported Achievement: %@", achievement);
    } else {
        NSLog(@"GCM Error while reporting achievement: %@", error);
    }
}

- (void)gameCenterManager:(GameCenterManager *)manager reportedScore:(GKScore *)score withError:(NSError *)error {
    if (!error) {
        NSLog(@"GCM Reported Score: %@", score);
    } else {
        NSLog(@"GCM Error while reporting score: %@", error);
    }
}

- (void)gameCenterManager:(GameCenterManager *)manager didSaveScore:(GKScore *)score {
    NSLog(@"Saved GCM Score with value: %lld", score.value);
}

- (void)gameCenterManager:(GameCenterManager *)manager didSaveAchievement:(GKAchievement *)achievement {
    NSLog(@"Saved GCM Achievement: %@", achievement);
}


#pragma mark GADRequest generation

// Here we're creating a simple GADRequest and whitelisting the application
// for test ads. You should request test ads during development to avoid
// generating invalid impressions and clicks.
- (GADRequest *)createRequest {
    GADRequest *request = [GADRequest request];
    
    // Make the request for a test ad. Put in an identifier for the simulator as
    // well as any devices you want to receive test ads.
    request.testDevices = [NSArray arrayWithObjects:@"af028767f1fdd1d565451af4c6b384c2", nil];
    return request;
}

#pragma mark GADBannerViewDelegate callbacks

// We've received an ad successfully.
- (void)adViewDidReceiveAd:(GADBannerView *)adView {
    NSLog(@"Received ad successfully");
}

- (void)adView:(GADBannerView *)view
didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    // Ignoring specific orientations we don't care about.
    if (orientation == UIDeviceOrientationFaceUp ||
        orientation == UIDeviceOrientationFaceDown ||
        orientation == UIDeviceOrientationUnknown) {
        return;
    }
    // On orientation changes we do care about, remove the Smart Banner entry and
    // replace it with the Smart Banner entry correct for the orientation we are
    // in.
   
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        self.adBanner.adSize = kGADAdSizeSmartBannerLandscape;
    } else if (UIInterfaceOrientationIsPortrait(orientation)) {
        self.adBanner.adSize = kGADAdSizeSmartBannerPortrait;
    }
   
}
@end
