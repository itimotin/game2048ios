//
//  main.m
//  Tile
//
//  Created by Timotin Vanea on 3/14/14.
//  Copyright (c) 2014 Timotin Vanea. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
